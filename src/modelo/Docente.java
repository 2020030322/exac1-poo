
package modelo;

public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int hrsImp;
    
    //Constructores
    //Vacio
    public Docente(){
        
    }
    //Parametros
    public Docente(int numDocente,String nombre,String domicilio,int nivel,float pagoBase,int hrsImp){
        this.numDocente = numDocente; 
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.hrsImp = hrsImp;        
    }
    //Copia
    public Docente(Docente Doc){
        this.numDocente = Doc.numDocente; 
        this.nombre = Doc.nombre;
        this.domicilio = Doc.domicilio;
        this.nivel = Doc.nivel;
        this.pagoBase = Doc.pagoBase;
        this.hrsImp = Doc.hrsImp;  
    }
    //Metodos Setter & Getter

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHrsImp() {
        return hrsImp;
    }

    public void setHrsImp(int hrsImp) {
        this.hrsImp = hrsImp;
    }
    //Metodos a calcular
    public float CalcularPago(){
        this.pagoBase = this.pagoBase * this.hrsImp;
        if(nivel==1){
            this.pagoBase *= 1.3; //Incremento del 30%
        }else if(nivel==2){
            this.pagoBase *=1.5;  //Incremento del 50%
        }else if(nivel==2){
            this.pagoBase *=1.5;  //Incremento del 100%
        }
        return pagoBase;
        
    }
    public double CalcularImpuesto(){
        this.pagoBase = CalcularPago();
        return pagoBase * 0.16;
        
    }
    public float CalcularBono(int cantidadhijos){
        return 0;
    
    }
}